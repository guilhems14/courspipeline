FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \
    apt-utils \
    curl \
    zip \
    unzip \    
    # apache
    apache2 \
    # PHP 8.1
    libapache2-mod-php8.1 \
    php8.1 \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Suppression du répertoire html
RUN rm -rf /var/www/html 

ADD src /var/www/
EXPOSE 80
WORKDIR /var/www/
CMD apachectl -D FOREGROUND 
